﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BoardBuilder : MonoBehaviour
{
    public static BoardBuilder instance = null; // Singleton like instance of Mapbuilder
    public GameObject pathBlock; // Prefabrication for a path block
    public GameObject groundBlock; // Prefabrication for a ground block
    public GameObject board; // Parent transform for the board

    public GameObject[, ] grid; // Grid to hold all blocks that make up the board
    public GameObject cam; // The player camera
    public GameObject momBot; // MomBot prefab to place on the board

    private List<GameObject> path; // The path MomBot will follow
    public GameObject endGameArea; // Prefab for MomBot's destination

    private BoardGeneratedEvent boardGeneratedEvent; // Event that fires when the board is generated

    private bool boardGenerated = false; // Whether the board has been generated

    private float coroutineWaitTime = 0.0001f;

    void Awake()
    {
        instance = this;
        boardGeneratedEvent = new BoardGeneratedEvent();
    }

    void Start()
    {
        // Determine the width and height of the board
        int random = UnityEngine.Random.Range(5, 15);

        // Generate a two dimensional array of that size
        grid = new GameObject[random, random];

        // Intiatilze a list object to hold Mombot's path
        path = new List<GameObject>();

        // Move the camera to where the center of the board will be so the player can see it being created
        cam.transform.position = new Vector3(random / 2, random, random / 2);

        // Create the path with a coroutine so the player can see it being created
        StartCoroutine(createPath(random - 1));
    }

    /// <summary>
    /// Creates a path for Mombot to follow
    /// </summary>
    /// <param name="random">Random number that gives the width and height of the board</param>
    IEnumerator createPath(int random)
    {
        // Start at zero in both dimensions
        int x = 0;
        int z = 0;

        // Loop while we are not past the top right block's index
        while (true)
        {
            // Create a path block at this x,z position
            grid[x, z] = Instantiate(pathBlock, new Vector3((float) x, 0f, (float) z), Quaternion.identity);
            // Set its parent transform to the board
            grid[x, z].transform.parent = board.transform;
            // Add this GameObject to the path list
            path.Add(grid[x, z]);

            // If we have reached the top right block
            if (x == random && z == random)
            {
                // Create the end game area box
                Instantiate(endGameArea, new Vector3((float) x, 1f, (float) z), Quaternion.identity);
                // Set its transform parent to the board
                transform.parent = board.transform;
                // Leave the while loop
                break;
            }
            yield return new WaitForSeconds(coroutineWaitTime);

            // Randomly move on the x or z axis
            if (UnityEngine.Random.Range(0, 100) <= 50)
            {
                if (x < random) x++;
            }
            else
            {
                if (z < random) z++;
            }
        }
        // Start the coroutine to fill the board
        StartCoroutine(fill(random));
    }

    /// <summary>
    /// Fills in the ground around the MomBot path
    /// with ground blocks to flesh out the board
    /// </summary>
    /// <param name="random">Random number that gives the width and height of the board</param>
    IEnumerator fill(int random)
    {
        // Double for loop to traverse the whole plain
        for (int i = 0; i <= random; i++)
        {
            for (int j = 0; j <= random; j++)
            {
                // If tis position is null it means there is no path block in it
                if (grid[i, j] == null)
                {
                    // Create a ground block at this position
                    grid[i, j] = Instantiate(groundBlock, new Vector3((float) i, 0f, (float) j), Quaternion.identity);
                    // Set its parent transform to the board
                    grid[i, j].transform.parent = board.transform;
                    yield return new WaitForSeconds(coroutineWaitTime);
                }

            }
        }

        // Place the MomBot on the board
        GameObject mom = Instantiate(momBot);

        // Place the camera at a position where can view the whole board
        cam.transform.position = new Vector3(0 - random / 4, random, 0 - random / 4);
        // Look at the center of the board
        cam.transform.LookAt(grid[random / 2, random / 2].transform);

        // Fire the board generated event
        boardGeneratedEvent.Invoke();
        boardGenerated = true;
    }

    /// <summary>
    /// Registers a function to be called when the board is generated
    /// </summary>
    /// <param name="call">Function to be called when the board is generated</param>
    public void RegisterForBoardGeneratedEvent(UnityAction call)
    {
        boardGeneratedEvent.AddListener(call);
    }

    /// <summary>
    /// Getter for boardGenerated
    /// </summary>
    public bool IsBoardGenerated()
    {
        return boardGenerated;
    }

    /// <summary>
    /// Getter for MomBot path
    /// </summary>
    public List<GameObject> GetPath()
    {
        return path;
    }
}