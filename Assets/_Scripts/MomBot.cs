﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MomBot : MonoBehaviour
{
    public static MomBot instance; // Singleton like instance of MomBot
    private List<GameObject> path; // MomBot's path

    // Transforms to act as start and end markers for the journey.
    public Transform startMarker;
    public Transform endMarker;

    // Movement speed in units/sec.
    public float speed = 1.0F;
    private float startTime; // Time when the movement started.
    private float journeyLength; // Total distance between the markers.
    private int steps = 0; // Current step number between targets
    public static int health = 25; // Total Mombot health 
    private int laserTowersAttached = 0; // Number of towers attacking Mombot
    private float lastCycleTime = 0f; // Last damage cycle time

    EndGameEvent endGameEvent; // Event that fires at the end of the game
    private float damageInterval = 1f; // Interval to damage MomBot

    private bool shouldMove = false; // Whetehr MomBot should be moving

    void Awake()
    {
        instance = this;
        endGameEvent = new EndGameEvent(); // Initialize the endgame event for use later
    }

    void Start()
    {
        // Get MomBot's path on the board
        path = BoardBuilder.instance.GetPath();
        // Keep a note of the time movement started.
        startTime = Time.time;

        // Set initial values for movement
        startMarker = path[0].transform;
        endMarker = path[0].transform;
        transform.position = path[0].transform.position;
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
    }

    private void Update()
    {
        MomBotDamageCycle(); // Run the damage cycle every frame

        // Start movement when player presses enter
        if (Input.GetKeyDown(KeyCode.Return))
        {
            shouldMove = true;
        }

        // If we should move Mombot... move MomBot
        if (shouldMove)
        {
            MoveMomBot();
        }
    }

    /// <summary>
    /// Damages MomBot on a time interval
    /// </summary>
    private void MomBotDamageCycle()
    {
        // Calculate how much time has passed since MomBot was damaged and damage MomBot if enough time has passed
        if (Time.time - lastCycleTime > damageInterval)
        {
            health = Mathf.Clamp(health - (laserTowersAttached), 0, health);
            lastCycleTime = Time.time; // Store this damage cycle time for future checking
        }

        // If MomBot has no health left destroy momBot and fire the end game event
        if (health <= 0)
        {
            endGameEvent.Invoke(EndState.Win);
            Destroy(gameObject);
        }

    }

    /// <summary>
    /// Moves MomBot smoothly along the board path with Lerp
    /// </summary>
    private void MoveMomBot()
    {

        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / journeyLength;

        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(startMarker.position, endMarker.position, fracJourney);

        // If MomBot has not reached the end of the path
        if (steps < path.Count)
        {
            // If we have reached the next block in the path
            if (transform.position == endMarker.position)
            {
                steps++; // Increase our total steps on the path
                startMarker = endMarker; // The new start marker is now the old destination
                endMarker = path[steps].transform; // End marker is the next block on the path
                // Calculate the new journeylength
                journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
                // Store the new start time
                startTime = Time.time;
                // Point MomBot at the new destination
                transform.LookAt(new Vector3(endMarker.transform.position.x, endMarker.transform.position.y, endMarker.transform.position.z));
            }
        }
        else
        {
            // If MomBot has traversed the whole path the player loses
            endGameEvent.Invoke(EndState.Lose); // fire the end game event
            Destroy(gameObject);
        }

    }

    /// <summary>
    /// Registers a function to be called when the game ends
    /// </summary>
    /// <param name="call">Function to be called when the game ends</param>
    public void RegisterForEndGameEvent(UnityAction<EndState> call)
    {
        endGameEvent.AddListener(call);
    }

    /// <summary>
    /// Increase the towers attacking MomBot
    /// </summary>
    public void increaseTowers()
    {
        laserTowersAttached++;
    }

    /// <summary>
    /// Decrease the towers attacking MomBot
    /// </summary>
    public void decreaseTowers()
    {
        laserTowersAttached--;
    }

    /// <summary>
    /// Getter for shouldMove
    /// </summary>
    public bool IsMomBotMoving()
    {
        return shouldMove;
    }
}