﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public Transform firepoint; // The point to fire the laser from
    public LineRenderer laser; // The linerenderer that acts as a laser
    public AudioSource beamSound; // The laser sound

    public Material laser1; // First laser mat
    public Material laser2; // Second laser mat
    private int frameNum = 0; // Running frame number
    private int laserState = 0; // Laser state for switching materials
    private bool attacking;
    private bool gameEnded = false; // Whether the game has ended
    private GameObject mom; // GameObject MomBot

    private float range = 5; // Range of tower
    private float yOffset = 1.5f; // y look offset

    void Awake()
    {
        mom = MomBot.instance.gameObject; // Get the gameobject MomBot script is attached to
        MomBot.instance.RegisterForEndGameEvent(GameEnded); // Register for the end game event
    }

    // Update is called once per frame
    void Update()
    {
        // If the board has been generated and the game is not over
        if (BoardBuilder.instance.IsBoardGenerated() && !gameEnded)
        {
            // Look at MomBot but make sure to alwys look at them same height
            transform.LookAt(new Vector3(mom.transform.position.x, transform.position.y, mom.transform.position.z));
            // Calculate the distance between mombot and the tower
            float dist = Vector3.Distance(new Vector3(mom.transform.position.x, mom.transform.position.y + yOffset, mom.transform.position.z), transform.position);

            // If MomBot is moving and within range
            if (dist <= range && MomBot.instance.IsMomBotMoving())
            {
                // If the tower is not already attacking start attacking
                if (!attacking)
                {
                    StartAttacking();
                }
                AnimateLaser(); // Change cosmetic elements of laser  
            }
            else
            {
                // If attacking, stop attacking
                if (attacking)
                {
                    StopAttacking();
                }
            }
        }
    }

    /// <summary>
    /// Enables the laser's cosmetic elements
    /// </summary>
    private void EnableLaser()
    {
        laser.enabled = true; // Enable the linerenderer
        beamSound.enabled = true; // Enable the laser sound effect
    }

    /// <summary>
    /// Disable the laser's cosmetic elements
    /// </summary>
    private void DisableLaser()
    {
        laser.enabled = false; // Disable the linerenderer
        beamSound.enabled = false; // Disable the sound effect
    }

    /// <summary>
    /// Animate the laser by moving its start and end point and cycling its material 
    /// </summary>
    private void AnimateLaser()
    {
        // Every other frame swap the material
        if (frameNum % 2 == 0)
        {
            if (laserState == 0)
            {
                laser.material = laser2;
                laserState = 1;
            }
            else
            {
                laser.material = laser1;
                laserState = 0;
            }
        }
        frameNum++;

        // Adjust the laser's start and end points
        laser.SetPositions(new Vector3[] { firepoint.position, new Vector3(mom.transform.position.x, firepoint.position.y, mom.transform.position.z) });

    }

    /// <summary>
    /// Start attacking MomBot
    /// </summary>
    private void StartAttacking()
    {
        attacking = true;
        // increase the number of towers attacking on MomBot
        MomBot.instance.increaseTowers();
        EnableLaser();
    }

    /// <summary>
    /// Stop Attacking MomBot
    /// </summary>
    private void StopAttacking()
    {
        DisableLaser();
        attacking = false;
        // Decrease the number of towers attacking on MomBot
        MomBot.instance.decreaseTowers();
    }

    /// <summary>
    /// Set gameEnded when End game event is recieved
    /// </summary>
    private void GameEnded(EndState state)
    {
        gameEnded = true;
    }
}