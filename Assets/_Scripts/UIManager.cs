﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private string endGameMessage = null; // Message to display to the user when game ends
    public GameObject tower; // Tower prefab to instantiate

    void Start()
    {
        // Register for the end game event
        BoardBuilder.instance.RegisterForBoardGeneratedEvent(RegisterForEndGameEvent);
    }

    void OnGUI()
    {
        GUI.skin.box.fontSize = 30;

        // If the game has not ended
        if (endGameMessage == null)
        {
            // Get the object selected by the user
            GameObject selObj = Selector.instance.GetSelectedObject();

            // Create a UI box to display game info
            Rect labelRect = new Rect(Screen.width / 5, 0f, Screen.width / 3, Screen.height / 10);
            GUIContent content = new GUIContent("Towers available: " + GameManager.instance.GetRemainingTowers() +
                "\n Mombot Health: " + MomBot.health);
            GUI.Box(labelRect, content);

            // If no object is selected and tower spawn is permitted
            if (selObj != null && GameManager.instance.CanSpawnTower())
            {
                // If the user clicks laser tower spawn one at their selected position
                Rect buttonRect = new Rect(Screen.width / 10, Screen.height - Screen.height / 10, Screen.width / 10, Screen.height / 10);
                if (GUI.Button(buttonRect, "Laser Tower"))
                {
                    Instantiate(tower, new Vector3(selObj.transform.position.x, selObj.transform.position.y + 1f, selObj.transform.position.z), Quaternion.identity);
                    GameManager.instance.IncreaseCurrentNumberOfTowers(); // Increase number of towers on the board
                }
            }
        }
        else
        {
            // If the game has ended display the end game message
            Rect labelRect = new Rect(0f, 0f, Screen.width, Screen.height);
            GUIContent content = new GUIContent(endGameMessage);
            GUI.Box(labelRect, content);
        }
    }

    /// <summary>
    /// Sets the end game message depending on what state was recieved in the end game event
    /// </summary>
    /// <param name="state">Win or loss state</param>
    void SetEndGameMessage(EndState state)
    {
        switch (state)
        {
            case EndState.Win:
                endGameMessage = "Mombot was destroyed! You win!";
                break;
            case EndState.Lose:
                endGameMessage = "Mombot made it! You lose!";
                break;
            default:
                endGameMessage = null;
                break;
        }
    }

    /// <summary>
    /// Register for the end game event when board generation event fires
    /// </summary>
    void RegisterForEndGameEvent()
    {
        MomBot.instance.RegisterForEndGameEvent(SetEndGameMessage);
    }
}