﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{

    public static GameManager instance = null; // Singleton like instance of GameObject
    private int numAllowedTowers = 5; // The maximum number of towers the user is permitted to put on the board
    private int currentNumTowers = 0; // The current number of towers on the board

    private void Awake()
    {
        instance = this; // Set the static instance to the instance that exists in the hierachy
    }

    void Update()
    {
        // If the player presses escape exit the game
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    /// <summary>
    /// Increases the count for the number of towers on the board
    /// </summary>
    public void IncreaseCurrentNumberOfTowers()
    {
        currentNumTowers++;
    }

    /// <summary>
    /// Whether a new tower is permitted to be spawned
    /// </summary>
    public bool CanSpawnTower()
    {
        return (currentNumTowers < numAllowedTowers);
    }

    /// <summary>
    /// Gets number of remaining towers a player can place
    /// </summary>
    public int GetRemainingTowers()
    {
        return numAllowedTowers - currentNumTowers;
    }
}