﻿using UnityEngine;
using UnityEngine.Events;

public enum EndState
{
    Win,
    Lose
}

[System.Serializable]
public class EndGameEvent : UnityEvent<EndState>
{

}