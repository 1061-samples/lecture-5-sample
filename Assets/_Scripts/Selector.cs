﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{
    public static Selector instance = null; // Singleton like instance of Selector
    public Material selectMaterial; // Material to apply when an object is selected
    private Material previousMaterial; // Material on an object before it was selected
    private GameObject selectedObject = null; // Object that is currently selected by the selector

    void Awake()
    {
        instance = this;
    }

    void Update()
    {
        // If the player left clicks and the board has been generated select what they clicked on
        if (Input.GetMouseButtonDown(0) && BoardBuilder.instance.IsBoardGenerated())
        {
            selectObject();
        }

        // If the player rights clicks deselect whatever object is selected, if any
        if (Input.GetMouseButtonDown(1) && BoardBuilder.instance.IsBoardGenerated())
        {
            deselectObject();
        }
    }

    /// <summary>
    /// Deselect the currently selected object
    /// </summary>
    private void deselectObject()
    {
        // If an object is selected restore its original material and set selectedObject to null
        if (selectedObject != null)
        {
            selectedObject.GetComponent<Renderer>().material = previousMaterial;
            selectedObject = null;
        }    
    }

    /// <summary>
    /// Select an object using current mouse location to Raycast
    /// </summary>
    void selectObject()
    {
        RaycastHit hit;
        // Create a ray with the user's click location
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // If that ray hits an object in the scene
        if (Physics.Raycast(ray, out hit))
        {
            // If an object is already selected restore its original material
            if (selectedObject != null)
            {
                selectedObject.GetComponent<Renderer>().material = previousMaterial;
                selectedObject = null;
            }

            // If the raycast hits a ground block
            if(hit.transform.gameObject.tag.Equals("ground"))
            {
                // Select the block the raycast hit
                selectedObject = hit.transform.gameObject;
                // Get the renderer from the selected object
                Renderer rend = selectedObject.GetComponent<Renderer>();
                // store the current material for restoration later
                previousMaterial = rend.material;
                // Apply the selection material
                rend.material = selectMaterial;
            }
        }
        else
        {
            // If the raycast hits nothing then restore the currently selected object's material \
            // and deselect the current selected object
            if (selectedObject != null)
            {
                selectedObject.GetComponent<Renderer>().material = previousMaterial;
            }
            deselectObject();
        }
    }

    /// <summary>
    /// Getter for selected object
    /// </summary>
    public GameObject GetSelectedObject()
    {
        return selectedObject;
    }
}